//Realizar conexión con la base de datos
const db = require('../util/database');



module.exports = class Incidente {

    constructor(id_incidente,id_lugar) {
       
        this.id_incidente = id_incidente, 
        this.id_lugar = id_lugar
        
    }

    //Este método servirá para guardar de manera persistente el nuevo objeto. 
    save() {
        // Almacenas el objeto que se acaba de crear
        return db.execute('INSERT INTO incidentes_lugares (id_incidente, id_lugar) VALUES (?, ?)',
        [this.id_incidente, this.id_lugar]
    );
        
    }
  
     
    static fetchAll() {
       return db.execute('SELECT IL.id_incidente,IL.id_lugar,tipo_incidente,nombre_lugar, fecha , (SELECT COUNT(*) FROM incidentes_lugares) AS totalIncidentes FROM incidentes I, lugares L, incidentes_lugares IL WHERE I.id_incidente=IL.id_incidente AND L.id_lugar=IL.id_lugar  ORDER BY fecha DESC' );       
    }
    
    static fetchOne(id){
        return db.execute('SELECT * FROM lugares WHERE id=?',[id]);   
    }

  
    
}