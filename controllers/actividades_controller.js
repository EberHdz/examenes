// Obtener lo que se va ser en MODELS
const { json } = require('body-parser');
const Incidente = require('../models/actividades');



exports.getNuevoRegistro = (request, response, next) => {

   response.render('nuevo_registro',{title:'Nuevo Registro',
                                    });
   
};


exports.postNuevoRegistro =  (request, response, next) => {

    console.log(request.body.id_incidente)
    console.log(request.body.id_lugar)
    
     const incidente=new Incidente( request.body.id_incidente, request.body.id_lugar);

     incidente.save().then(() => {
         response.redirect('/actividades');
     }).catch(
         err => { console.log(err)
         //Si existe error, regresa a la forma
         response.redirect('/actividades/nuevo_registro');}
   
     );
     
 };

/////////////////////////////////////////////////////////////////////////////////////////////    

exports.get= (request, response, next) => {
 
             
    //response.sendFile(path.join(__dirname,'..','views', 'mascotas.html'));
    Incidente.fetchAll() 
           .then(([rows, fieldData]) => {
               
            response.render('actividades',
            {incidentes: rows , 
             totalIncidentes: rows[0].totalIncidentes,
             title: 'Actividades' 

             });
             })
    //Saber si te equivocaste en alguna parte:
            .catch(err => { 
             console.log(err);
            });
        
    
        


};
