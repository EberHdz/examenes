-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-05-2021 a las 17:26:50
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jurassic_park`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidentes`
--

CREATE TABLE `incidentes` (
  `id_incidente` int(11) NOT NULL,
  `tipo_incidente` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `incidentes`
--

INSERT INTO `incidentes` (`id_incidente`, `tipo_incidente`) VALUES
(1, 'falla electrica'),
(2, 'fuga de herbivoro'),
(3, 'fuga de velociraptors'),
(4, 'fuga de trex'),
(5, 'robo de ADN'),
(6, 'auto descompuesto'),
(7, 'visitantes en zona no autorizada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidentes_lugares`
--

CREATE TABLE `incidentes_lugares` (
  `id_incidente` int(11) NOT NULL,
  `id_lugar` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `incidentes_lugares`
--

INSERT INTO `incidentes_lugares` (`id_incidente`, `id_lugar`, `fecha`) VALUES
(1, 2, '2021-05-05 14:05:47'),
(5, 5, '2021-05-05 15:10:30'),
(6, 7, '2021-05-05 14:05:47'),
(7, 7, '2021-05-05 15:14:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

CREATE TABLE `lugares` (
  `id_lugar` int(11) NOT NULL,
  `nombre_lugar` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lugares`
--

INSERT INTO `lugares` (`id_lugar`, `nombre_lugar`) VALUES
(1, 'centro turistico'),
(2, 'laboratorios'),
(3, 'restaurante'),
(4, 'centro operativo'),
(5, 'triceratops'),
(6, 'dilofosaurios'),
(7, 'velociraptors'),
(8, 'trex'),
(9, 'planicie de los herbivoros');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `incidentes`
--
ALTER TABLE `incidentes`
  ADD PRIMARY KEY (`id_incidente`);

--
-- Indices de la tabla `incidentes_lugares`
--
ALTER TABLE `incidentes_lugares`
  ADD PRIMARY KEY (`id_incidente`,`id_lugar`),
  ADD KEY `FK_id_incidente` (`id_incidente`),
  ADD KEY `FK_id_lugar` (`id_lugar`);

--
-- Indices de la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD PRIMARY KEY (`id_lugar`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `incidentes`
--
ALTER TABLE `incidentes`
  MODIFY `id_incidente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `lugares`
--
ALTER TABLE `lugares`
  MODIFY `id_lugar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `incidentes_lugares`
--
ALTER TABLE `incidentes_lugares`
  ADD CONSTRAINT `incidentes_lugares_ibfk_1` FOREIGN KEY (`id_incidente`) REFERENCES `incidentes` (`id_incidente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incidentes_lugares_ibfk_2` FOREIGN KEY (`id_lugar`) REFERENCES `lugares` (`id_lugar`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
