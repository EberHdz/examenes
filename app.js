const express = require('express');
const app = express(); //Creando aplicación utilizando express.
/////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////
    const bodyParser = require('body-parser');//Poder trabajar con mas facilmente con peticiones y formularios.
app.use(bodyParser.urlencoded({extended: false}));


const router=express.Router(); // Para utilizar métodos get y post.

// Para los estilos y funcionalidades de CSS y JS, y no se tenga que crear una ruta para cada uno.
const path= require('path');
app.use(express.static(path.join(__dirname, 'public')));
/////////////////////////////////////////////////////////
//Para poder utilizar las cookies
const cookieParser = require('cookie-parser');
app.use(cookieParser());
/////////////////////////////////////////////////////////
//AJAX-JSONS
app.use(bodyParser.json());


/////////////////////////////////////////////////////////
//Poder utilizar sessiones
const session = require('express-session');

app.use(session({
    secret: 'cjKepemdlOIDHKLSÑIDMkmdsñlmlkdfmlk129i309jmnaslklkmclkdñlkñsalLKMLklkmLKmLjkvtdfpassdegjhknjweyjkskwhfiNkjnknlkjnkVTFcmlkmlkmdAKLdAKDDADñ', //Permite encriptar la sesión.
    resave: false, //La sesión no se guardará en cada petición, sino sólo se guardará si algo cambió 
    saveUninitialized: false, //Asegura que no se guarde una sesión para una petición que no lo necesita
}));

/////////////////////////////////////////////////////////
//Protección contra CSRF 

const csrf= require('csurf');
const csrfProtection= csrf();
const csrfMiddleware = require('./util/csrf');
app.use(csrfProtection);
app.use(csrfMiddleware);


/////////////////////////////////////////////////////////
//Variable Global de TEMPLATES
app.set('view engine', 'ejs');
app.set('views', 'views'); // En que carpeta estan los
/////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////// RUTAS ///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////
const rutasActividades = require('./routes/actividades.js');
app.use('/actividades', rutasActividades);
///////////////////////////////////////////////////////////////
//Una vez que se crea las rutas de mascotas en la carpeta routes, se agrega el módulo de mascotas en el archivo principal
///////////////////////////////////////////////////////////////// Ruta para la Raíz de la Página:
app.get('/', (request, response, next) => {
    console.log(request.body);
    response.send('ESTA ES LA PÁGINA PRINCIPAL.'); 
});

///////////////////////////////////////////////////////////////
// Poner un 404 a todas las rutas que son incorrectas:
app.use((request, response, next) => {
    console.log('Error 404');
    response.status(404);
    response.send('Lo sentimos esta página no existe!'); //Manda la respuesta
});

app.listen(9000);