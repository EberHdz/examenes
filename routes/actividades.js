const express = require('express');

const router = express.Router();

//Para poder mandar archivos html completos.
const path= require('path');


//CONTROLADORES
const actividades_controller=require('../controllers/actividades_controller');


////////////////////////////////////////////////////////////

//Fue la primera forma de agregar un nuevo perro a la lista:
router.get('/nuevo_registro', actividades_controller.getNuevoRegistro);  //Ahora ya se cambio por el controlador.

//Creando rutas utilizando node
router.post('/nuevo_registro',actividades_controller.postNuevoRegistro);

router.get('/', actividades_controller.get );





module.exports = router;